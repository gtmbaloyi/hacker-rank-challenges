import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the countingValleys function below.
    private static int countingValleys(int n, String s) {
        int seaLevel = 0;
        int level = 0;
        int numValleys = 0;

        for (int x = 0; x < n; x++) {
            if (level == seaLevel) {
                if (s.charAt(x) == 'D') {
                    numValleys++;
                    level--;
                }
                else {
                    level++;
                }
            } else {
                if (s.charAt(x) == 'D') {
                    level--;
                } else {
                    level++;
                }
            }


        }
        return numValleys;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new      FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        int result = countingValleys(n, s);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
